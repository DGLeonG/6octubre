<?php

namespace App\Controllers;
use App\Models\GrupoModel;

class Home extends BaseController
{
    public function mostrarGrupos()
    {
        $grupos = new GrupoModel();
        
         $textoabuscar = $this->request->getPost('texto');
        
        $data['title'] = "Tabla Guille";
        $data['result'] = $datos = $grupos->like(['codigo'=>strtoupper($textoabuscar)])->findAll();
        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/
        
        return view('alumnos/lista',$data);
    }
    
    public function formMostrarGrupos(){
        $data['title'] = "Tabla Guille";
        return view('alumnos/form',$data);
    }

}
